#! /usr/bin/env python

'''
Copyright (c) 2010-2012 Daniel Foerster/Dsigner Software <pydsigner@gmail.com>

Celestron is a space shooter inspired by Chromium-BSU. To create Celestron, 
many of Chromium-BSU's images and sounds were used, but the code was written 
from scratch in Python and many features added.
--------------------------------------------------------------------------------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

Version 7 brings a whole new level of sophistication to Celestron, with a main 
menu, a new vector system, much more gameplay, and much of the infrastructure 
for advanced scenarios.
'''
import pgpu.math_utils as p_mu
from pygu import pygw

from common import *


VERSION = '7.1.2'
MAIN, PAUSED, PLAYING, WON, LOST, INIT, CLEARED = range(7)
F_QUIT = 10


fsave = data_dir + '/player_7.save'
fhigh = data_dir + '/scores_7.save'
if not os.path.isfile(fsave):
    open(fsave, 'w').close()
if not os.path.isfile(fhigh):
    open(fhigh, 'w').close()

##### Event callbacks ##################

def quitter(eman, gstate, event):
    raise eman.Message(F_QUIT)


def escaper(eman, gstate, event):
    if event.key == K_ESCAPE:
        if gstate.in_game:
            # This is just too evil... plus it's slower
            # gstate.state = (PAUSED if gstate.state == MAIN 
            #                 else (MAIN, gstate.data.set_music('menu'))[0])
            if gstate.state == MAIN:
                gstate.state = PAUSED
            else:
                gstate.state = MAIN
                gstate.data.set_music('menu')
        else:
            raise eman.Message(F_QUIT)


def screenshooter(eman, gstate, event):
    if event.key == K_i:
        screenshot(gstate.screen, VERSION)


def volume_control(eman, gstate, event):
    if event.type == 'music_up' or event.key in [K_KP_PLUS, K_EQUALS]:
        gstate.data.set_m_vol(.02, True)
    elif event.type == 'music_down' or event.key in [K_KP_MINUS,  K_MINUS]:
        gstate.data.set_m_vol(-.02, True)
    elif event.type == 'sound_up' or event.key == K_PERIOD:
        gstate.data.set_s_vol(.02, True)
    elif event.type == 'sound_down' or event.key == K_COMMA:
        gstate.data.set_s_vol(-.02, True)


def flip_full(eman, gstate, event):
    if event.type == 'flip_fullscreen' or event.key == K_F11:
        pygame.display.toggle_fullscreen()


def next_song(eman, gstate, event):
    gstate.data.next_song()


def next_level(eman, gstate, event):
    if event.mod & KMOD_CTRL and event.key == K_n:
        if gstate.state in [WON, LOST]:
            gstate.in_game = False
            gstate.state = MAIN
            gstate.data.set_music('menu')
        elif gstate.state == CLEARED:
            gstate.state = INIT


def flip_pause(eman, gstate, event):
    if event.key in [K_p, K_PAUSE]:
        gstate.state = PLAYING if gstate.state == PAUSED else PAUSED


def start_clicked(eman, gstate, event):
    setup_levelset(gstate, None)    # Use the default levelset
def quit_clicked(eman, gstate, event):
    eman.event('quit')
def full_clicked(eman, gstate, event):
    eman.event('flip_fullscreen')


def move_hero(eman, gstate, event, real):
    if real:
        hero = list(gstate.get_group('heroes'))[0]
        where = Vector(event.pos)
        cur = hero.rect.center
        trans = where - cur
        # Make sure we are actually going somewhere
        # This also avoids Div0 errors
        if trans.length:
            # limit the movement to our speed
            trans.length = p_mu.limit(trans.length, -hero.speed, hero.speed)
            # Move the rect!
            hero.rect.move_ip(trans)

##### Main loop ########################

def save_score(gstate):
    if gstate.score:
        gstate.scores.append([player_name, gstate.score, gstate.level])
        score_lines = ['\t'.join([str(i) for i in l]) 
                for l in sorted(gstate.scores, key = score_sort, reverse = 1)]
        with open(fhigh, 'w') as f:
            f.writelines([l + '\n' for l in score_lines])


def setup_levelset(gstate, levelset):
    # If we already were playing a game, save the score
    save_score(gstate)
    if not levelset or not gstate.levels.exists(levelset):
        levelset = 'classic'
    
    w, h = gstate.screen.get_size()
    list(gstate.get_group('heroes'))[0].rect.midtop = [w / 2, h - 80]
    
    gstate.levelset = levelset
    gstate.level = 1
    gstate.score = 0
    gstate.state = INIT
    gstate.in_game = True


def main(conf, title, res, levelset = ''):
    
    #### First init ####
    
    gstate = pyramid.StateManager(
            [MAIN, PAUSED, PLAYING, WON, LOST, INIT, CLEARED])
    
    heroes = pygame.sprite.Group()
    hero_missiles = pygame.sprite.Group()
    bads = pygame.sprite.Group()
    bad_missiles = pygame.sprite.Group()
    anims = pygame.sprite.Group()
    
    gstate.add_groups({'heroes': heroes, 'hero_missiles': hero_missiles, 
            'bads': bads, 'bad_missiles': bad_missiles, 'anims': anims, 
            'menu_widgets': pygw.Container(gstate[MAIN], (0, 0))})
    
    gstate.data = pyramid.Resources(24)
    
    gstate.conf = conf
    gstate.in_game = False
    gstate.score = gstate.level = 0
    gstate.scores = [[i.split()[0], int(i.split()[1]), i.split()[2]] 
            for i in open(fhigh).readlines()]
    S_VOL, M_VOL = conf['s_vol'] / 100., conf['m_vol'] / 100.
    
    # CODE SNIPPET FROM EXAILE
    if sys.platform == 'linux2':
        # Set process name.  Only works on Linux >= 2.1.57.
        try:
            import ctypes
            libc = ctypes.CDLL('libc.so.6')
            libc.prctl(15, title.lower(), 0, 0, 0) # 15 = PR_SET_NAME
        except:
            pass
    # END CODE SNIPPET
    
    screen = pygame.display.set_mode(res)
    pygame.display.set_caption(title)
    
    gstate.screen = screen
    
    clock = pygame.time.Clock()
    mod10 = 0
    
    gstate.area = screen.get_rect()
    gstate.state = MAIN
    
    #### Fonts #########################
    
    fn = os.path.join(data_dir, 'fonts', 'chintzy.ttf')
    mfn = os.path.join(data_dir, 'fonts', 'Engadget.ttf')
    font = pygame.font.Font(fn, 20)
    big_font = pygame.font.Font(fn, 30)
    big_font.set_underline(1)
    menu_font = pygame.font.Font(mfn, 18)
    
    #### Images ########################
    
    gstate.data.load_objects(
            [os.path.join(data_dir, dr) for dr in 
                ['images', 'music', 'playlists', 'sounds', 'core_code', 'code']
            ], 
            {'gstate': gstate})
    
    try:
        icon = gstate.data.get_image('icon2-7', 'other')
        pygame.display.set_icon(icon)
    except IndexError:
        Print('No icon available!')
    
    message_imgs = {}
    message_imgs[CLEARED] = gstate.data.get_image('cleared', 'other')
    message_imgs[WON] = gstate.data.get_image('victory', 'other')
    message_imgs[LOST] = gstate.data.get_image('fail', 'other')
    message_imgs[PAUSED] = gstate.data.get_image('paused', 'other')
    
    #### Menu ##########################
    
    overlay = gstate.data.get_image('backdrop-shade', 'other')
    
    bg = gstate.data.get_image('backdrop', 'other')
    sr = screen.get_rect()
    br = bg.get_rect()
    asize = ((sr.w // br.w + 1) * br.w, (sr.h // br.h + 1) * br.h)
    menu_background = pygame.Surface(asize)
    for x in range(0, asize[0], br.w):
        for y in range(0, asize[1], br.h):
            menu_background.blit(bg, (x, y))
    
    menu_background.blit(pygame.transform.scale(overlay, res), (0, 0))
    
    x, y = sr.w // 2, 200
    
    for text, cb in [('Start Game', start_clicked), 
            ('Flip Fullscreen', full_clicked), ('Quit', quit_clicked)]:
        
        t = menu_font.render(text, True, C_WHITE)
        img_n = gstate.data.get_image('button', 'other').copy()
        img_f = gstate.data.get_image('button-active2', 'other').copy()
        center_blit(img_n, t, (0, -3))
        center_blit(img_f, t, (0, -3))
        b = pygw.Button(gstate.get_group('menu_widgets'), 
                (x, y), (img_n, img_f), cb)
        b.rect.centerx = x
        y += 5 + b.rect.h
    
    yoff = y + 50 - sr.h // 2
    center_blit(menu_background, 
            gstate.data.get_image('chrome', 'logos'), (0, yoff))
    
    #### Levels ########################
    
    gstate.levels = LevelBank(gstate)
    gstate.levels.load_sets([os.path.join(data_dir, 'levels')])
    
    #### Hero ##########################
    
    hero = gstate.data.get_code('hero-01')([res[0] / 2, res[1] - 80], 
            gstate, 18)
    hero.health = conf['health']
    wnum = 0
    for c in [hero.weap_s, hero.weap_d, hero.weap_a]:
        c.rounds = conf['w%s' % wnum]
        wnum += 1
    
    #### Audio #########################
    
    gstate.data.set_m_vol(M_VOL)
    gstate.data.set_s_vol(S_VOL)
    try:
        gstate.data.set_music('menu')
        pygame.mixer.music.set_endevent(MUSIC_CHANGE)
    except IndexError:
        Print('No music found!')
    
    #### Events ########################
    
    # global events
    for s in gstate.get_states().values():
        s.bind(quitter, QUIT)
        s.bind(quitter, 'quit')
        s.bind(escaper, KEYDOWN)
        s.bind(screenshooter, KEYDOWN)
        s.bind(volume_control, KEYDOWN)
        s.bind(flip_full, KEYDOWN)
        s.bind(flip_full, 'flip_fullscreen')
        s.bind(next_song, MUSIC_CHANGE)
        s.bind(next_song, 'music_up')
        s.bind(next_song, 'music_down')
        s.bind(next_song, 'sound_up')
        s.bind(next_song, 'sound_down')
        s.bind(next_level, KEYDOWN)
    
    for s in [PAUSED, PLAYING]:
        gstate[s].bind(flip_pause, KEYDOWN)
    
    gstate[PLAYING].hotspot.add_static((None, move_hero), screen.get_rect())
    
    while 1:
        #### Process events ################
        flag = gstate.loop(pygame.event.get())
        if flag == F_QUIT:
            Print('You quit with score of %s!' % gstate.score)
            save_score(gstate)
            return dict(s_vol = S_VOL * 100, m_vol = M_VOL * 100)
        
        keystate = pygame.key.get_pressed()
        
        if gstate.state == INIT:
            
            gstate.data.set_music('normal')
            
            #### Build background and logo #####
            
            randbg = rdm.choice(gstate.data.images['bgs'].values())
            sr = screen.get_rect()
            br = randbg.get_rect()
            asize = ((sr.w // br.w + 1) * br.w, (sr.h // br.h + 1) * br.h)
            background = pygame.Surface(asize)
            for x in range(0, asize[0], br.w):
                for y in range(0, asize[1], br.h):
                    background.blit(randbg, (x, y))
            background = ScrollingBG(background, 
                        rdm.choice(gstate.data.images['logos'].values()), 3)
            #logo = rdm.choice(gstate.data.images['logos'].values())
            
            #### Reset groups ##################
            for g in ['hero_missiles', 'bads', 'bad_missiles', 'anims']:
                gstate.get_group(g).empty()
            
            #### Load level ####################
            
            lvl = gstate.levels.get_level(
                    '%s.%02d' % (gstate.levelset, gstate.level))
            bns = lvl.bonus
            hero.health += bns.get('health', 0)
            wnum = 0
            for c in [hero.weap_s, hero.weap_d, hero.weap_a]:
                c.rounds += bns.get('w%s' % wnum, 0)
                wnum += 1
            
            #Print(lvl.baddies)
            for b in lvl.baddies:
                bads.add(b)
            
            gstate.state = PAUSED       # Don't reenter init!
        
        #### Set the pace ##################
        tickcount = 50 if gstate.state == PLAYING else 35
        if not pygame.display.get_active():
            tickcount = 10
        clock.tick(tickcount)
        hright = hdown = 0
        mod10 %= 10
        if not mod10:
            frames = round(clock.get_fps(), 1)
        
        #### Now the real work! ############
        
        if gstate.state == MAIN:
            screen.blit(menu_background, (0, 0))
            gstate.get_group('menu_widgets').draw(screen)
        else:
            background.showl = gstate.state == PLAYING 
            background.update()
            screen.blit(background, (0, 0))
        
        if gstate.state == PLAYING:
            
            hright = keystate[K_RIGHT] - keystate[K_LEFT]
            hdown = keystate[K_DOWN] - keystate[K_UP]
            hero.move([hright, hdown])
            hero.fire(keystate)
            hero_missiles.update()
            bads.update()
            bad_missiles.update()
            
            for m, b in gnnmi_collide(hero_missiles, bads):
                gstate.score += sum([t.hit(m) for t in b])
                [m.hit(t) for t in b]
            
            for h, b in gnnmi_collide(heroes, bads):
                [h.collide(t) for t in b]
                gstate.score += sum([t.collide(h) for t in b])
            
            for m, h in gnnmi_collide(bad_missiles,heroes):
                [t.hit(m) for t in h]
                [m.hit(t) for t in h]
            
            for b in bads:
                if b.rect.top > gstate.area.bottom:
                    b.kill()
                    hero.health -= 200
            
            anims.update()
            if not bads:
                gstate.level += 1
                if gstate.levels.exists(
                        '%s.%02d' % (gstate.levelset, gstate.level)):
                    gstate.state = CLEARED
                    gstate.data.set_music('clear')
                else:
                    gstate.state = WON
                    gstate.data.set_music('win')
                
            elif hero.health <= 0:
                gstate.state = LOST
                gstate.data.set_music('lost')
        
        framerate = font.render(str(frames), 1, C_WHITE)
        frameratepos = framerate.get_rect(right = screen.get_width() - 10)
        screen.blit(framerate, frameratepos)
        
        if gstate.state != MAIN:
            
            # Draw missiles, ships, etc.
            
            heroes.draw(screen)
            bads.draw(screen)
            anims.draw(screen)
            hero_missiles.draw(screen)
            bad_missiles.draw(screen)
            
            r = []
            for c in [hero.weap_a, hero.weap_s, hero.weap_d, hero.weap_sp]:
                c = c.rounds
                r.append('INF' if c == -1 else str(c))
            roundcount = font.render('AMMO: ' + '; '.join(r), 1, C_RED)
            roundpos = roundcount.get_rect(left = 10)
            
            herohp = font.render('HP: ' + str(hero.health), 1, C_GREEN)
            herohppos = herohp.get_rect(left = roundpos.right + 15)
            
            scoredisp = font.render('Score: ' + str(gstate.score), 1, C_RED)
            scorepos = scoredisp.get_rect(left = herohppos.right + 15)
            
            lvl = font.render('LEVEL %s' % gstate.level, 1, C_BLUE)
            lvlpos = lvl.get_rect(left = scorepos.right + 15)
            
            badhp = font.render('Enemy HP: %s' % sum([e.health for e in bads]), 
                    1, C_RED)
            badhppos = badhp.get_rect(left = lvlpos.right + 30)
            
            screen.blit(roundcount, roundpos)
            screen.blit(herohp, herohppos)
            screen.blit(scoredisp, scorepos)
            screen.blit(lvl, lvlpos)
            screen.blit(badhp, badhppos)
            
            if gstate.state in message_imgs:
                # darken/fade the screen
                srf = pygame.Surface(screen.get_size())
                srf.fill(C_BLACK)
                srf.set_alpha(100)
                screen.blit(srf, (0, 0))
                
                srect = screen.get_rect()
                
                # select and draw our graphic
                uimg = message_imgs[gstate.state]
                urect = uimg.get_rect(center = srect.center).move(0, -50)
                urect.clamp(srect)
                screen.blit(uimg, urect)
                
                if gstate.state == PAUSED:
                    # draw a "menu"
                    pad = 5
                    head = big_font.render('HELP/MENU', 1, C_BLUE)
                    hrect = head.get_rect(midtop = urect.midtop, 
                            top = urect.bottom + 40)
                    screen.blit(head, hrect)
                    entries = ['Press [Esc] to pause game and use main menu.',
                    'Press [P] or [Pause] to pause/unpause the game.',
                    ('Press [Ctrl]+[N] to begin new level ' + 
                        'if current one is complete.'),
                    'Press [F11] to toggle fullscreen mode.'
                    'Press [I] to take a screenshot.',
                    'Press [A], [S], [D], and [Space] to fire the guns.',
                    ('Press [+] or [-] to increase or decrease the music ' +
                    'volume (currently %s)' % int(gstate.data.m_vol * 100)),
                    ('Press [>] or [<] to increase or decrease the SFX ' +
                    'volume (currently %s)') % int(gstate.data.s_vol * 100)]
                    prev = hrect
                    for e in entries:
                        rnd = menu_font.render(e, 1, C_WHITE)
                        loc = rnd.get_rect(top=prev.bottom+pad, 
                            midtop=srect.midtop)
                        # find rect
                        screen.blit(rnd, loc)
                        prev = loc
        
        pygame.display.update()
        mod10 += 1

if __name__ == '__main__':
    save = load_conf(open(fsave))
    cnf = dict(default_conf)
    cnf.update(save)
    
    rcnf = main(cnf, title = TITLE + ' ' + VERSION, res = (1000, 750))
    
    with open(fsave, 'w') as f:
        f.writelines(l + '\n' for l in dump_conf(rcnf))
