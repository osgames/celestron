"Pydsigner's Generic Python Utilities (PGPU) -- a collection of handy modules" 
'''and packages for Python.

__init__        --  version information
compatibility   --  module to make it easier to write modules compatible with 
                    both 2.x and 3.x
iter_utils      --  generic iterable utilities
file_utils      --  file utility module
math_utils      --  utilities to work with bases plus some trig utilities
security        --  security and encoding module
time_widgets    --  tkinter time widgets
tk_utils        --  tkinter widgets and helper functions
wrappers        --  utility value wrappers for places requiring functions

tkinter2x       --  2.x and 3.x compatibility layer for tkinter


This file provides version information.

AUTHORS:
v0.2.0+             --> pydsigner
v1.0.0+             --> pydsigner
v1.0.3+             --> pydsigner
'''

__version__ = '1.0.6'
