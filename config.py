from os import path, environ

# Where are all the data and saves?
data_dir = path.abspath(path.join(path.dirname(__file__), 'data'))

# What is the player's name?
player_name = environ.get('LOGNAME', environ.get('USER', 'player'))

## Load the users customizations
try:
    from custom_config import *
except ImportError:
    # No custom config, no problem!
    pass
