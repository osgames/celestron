import os
import importlib
import sys
import time
import random
import math

import pygame
from pygame.constants import *
from pgpu.math_utils import Vector
from pgpu.file_utils import parse_conf, make_conf
from pgpu.compatibility import *
from pygu import pyramid
from pygu.common import center_blit

from config import *


TITLE = 'Celestron'

MUSIC_CHANGE = 25

C_WHITE = (250, 250, 250)
C_GREEN = (0, 165, 0)
C_RED = (250, 50, 25)
C_BLUE = (30, 144, 255)
C_BLACK = (5, 5, 5)
C_GOLD = (205, 170, 3)
C_SILVER = (160, 160, 160)

default_conf = dict(health=1500, w0=90, w1=50, w2=85, s_vol=12, m_vol=16)


def tile(surf, size):
    """
    Tile @surf into a new Surface @size big.
    """
    new = pygame.Surface(size, pygame.SRCALPHA)
    r = surf.get_rect()
    
    for x in range(0, size[0], r.w):
        for y in range(0, size[1], r.h):
            new.blit(surf, (x, y))
    
    return new


def exactile(surf, size):
    """
    Tile @surf into a surface at least @size big, in such a way that the 
    resulting surface is itself tileable.
    For instance, tile()ing a 100x100 surface into a 350x350 surface will 
    result in having a surface that is not neccessarily tilable. 
    But exactile()ing that same surface into a 350x350 surface will result in a 
    400x400 surface which will be tilable (if the original surface is, that 
    is).
    """
    w, h = surf.get_size()
    new_w = int(math.ceil(size[0] / float(w))) * w
    new_h = int(math.ceil(size[1] / float(h))) * h
    return tile(surf, (new_w, new_h))


def color_to_alpha(surf, color):
    """
    Convert every occurence of @color in @surf to alpha ((0, 0, 0, 0), to be 
    exact), in-place.
    """
    px = pygame.PixelArray(surf)
    px.replace(color, (0, 0, 0, 0))


def build_enclosure(gstate, size, **kw):
    """
    bg     -- Just the background. Should probably be tilable.
    
    corner -- The top-left corner of the frame. Will be flipped about to get 
              the other corners.
    
    cutout -- Will be cut out of the background. Should be one color which is 
              not contained in the background. If this is None, this edge 
              shaping will not occur.
    
    edge   -- The top edge of the frame. Will be flipped to get the left edge, 
              and also the right and bottom edges if edge2 is not given.
    
    edge2  -- The right and bottom edge of the frame. Should be oriented for 
              the top edge, however.
    
    group  -- The group that these images should be taken from.
    
    offset -- The amount of padding that the background should receive.
    """
    bg = kw.pop('bg', 'texture-brushed')
    corner = kw.pop('corner', 'border-corner')
    cutout = kw.pop('cutout', 'rounded-corner-cutout')
    edge = kw.pop('edge', 'border-edge')
    edge2 = kw.pop('edge2', edge)
    group = kw.pop('group', 'ui')
    offset = kw.pop('offset', 2)
    if kw:
        raise ValueError('Too many kw args!')
    
    main = pygame.Surface(size, pygame.SRCALPHA)
    
    if cutout is not None:
        nwc = gstate.data.get_image(cutout, group)
        nec = pygame.transform.flip(nwc, True, False)
        swc = pygame.transform.flip(nwc, False, True)
        sec = pygame.transform.flip(swc, True, False)
        
        bg = tile(gstate.data.get_image(bg, group), Vector(size) - offset)
        
        ox, oy = Vector(bg.get_size()) - nwc.get_size()
        bg.blit(nwc, (0, 0))
        bg.blit(nec, (ox, 0))
        bg.blit(swc, (0, oy))
        bg.blit(sec, (ox, oy))
        
        color_to_alpha(bg, nwc.get_at((0, 0)))
    
    main.blit(bg, (offset, offset))
    
    n = gstate.data.get_image(edge, group)
    w = pygame.transform.rotate(n, 90)
    s = pygame.transform.flip(gstate.data.get_image(edge2, group), True, False)
    e = pygame.transform.rotate(s, 90)
    
    nw = gstate.data.get_image(corner, group)
    ne = pygame.transform.flip(nw, True, False)
    sw = pygame.transform.flip(nw, False, True)
    se = pygame.transform.flip(sw, True, False)
    
    wd, hd = nw.get_size()
    thick = n.get_height()
    
    frame = pygame.Surface(size, pygame.SRCALPHA)
    
    frame.blit(nw, (0, 0))
    frame.blit(ne, (size[0] - wd, 0))
    frame.blit(sw, (0, size[1] - hd))
    frame.blit(se, (size[0] - wd, size[1] - hd))
    
    hor_size = (size[0] - wd * 2, thick)
    n = tile(n, hor_size)
    s = tile(s, hor_size)
    
    vert_size = (thick, size[1] - hd * 2)
    w = tile(w, vert_size)
    e = tile(e, vert_size)
    
    frame.blit(n, (wd, 0))
    frame.blit(s, (wd, size[1] - thick))
    frame.blit(w, (0, hd))
    frame.blit(e, (size[0] - thick, hd))
    
    main.blit(frame, (0, 0))
    
    return main


def screenshot(surf, version):
    try:
        tm = time.strftime('%d.%m.%Y-%H.%M.%S')
        d = path.join(data_dir, 'screenshots')
        if not path.exists(d):
            os.mkdir(d)
        fname = path.join(d, '%s%s-%s.png' % (TITLE.lower(), version, tm))
        Print('Saving screenshot as %s...' % fname)
        pygame.image.save(surf, fname)
        Print('Save successful!')
    except Exception as e:
        Print('Save failed because: %s' % e)


def collide_mask_opt(left, right):
    """
    The same as pygame.sprite.collide_mask(), only optimized by doing rect 
    checking and de-generalized by assuming a .mask attribute
    """
    # If the rects do not overlap, the masks will not either, so skip this 
    # intensive process.
    if not left.rect.colliderect(right.rect):
        return False
    
    # In our case, we know that we have a .mask attribute. So let's save a few 
    # try-except blocks, etc.
    return left.mask.overlap(right.mask, (right.rect[0] - left.rect[0], 
                                          right.rect[1] - left.rect[1]))


def gnnmi_collide(g1, g2):
    """
    gnnmi = "Group Nokill Nokill Mask Items"
    
    No longer does this call pygame.sprite.groupcollide(), this is way more 
    optimized.
    """
    collisions = []
    for s1 in g1:
        scollides = [s2 for s2 in g2 if collide_mask_opt(s1, s2)]
        if scollides:
            collisions.append((s1, scollides))
    return collisions


def missile_collide(mg, sg):
    """
    A hyper-optimized missile-ship collider.
    """
    collisions = []
    sg = [s for s in sg if s.in_bounds]
    for m in mg:
        for s in sg:
            if collide_mask_opt(m, s):
                collisions.append((m, s))
                break
    return collisions


def load_conf(lines):
    raw = parse_conf(lines)
    res = {}
    for k in raw:
        res[k] = int(round(float(raw[k][0])))
    return res


def dump_conf(data):
    raw = {}
    for k in data:
        raw[k] = [str(data[k])]
    return make_conf(raw)


def score_sort(L):
    return L[1]


class ArcadeLevel(object):
    bonuses = {'health': 1, 'w0': 5.5, 'w1': 9.1, 'w2': 11.1}
    boss_ops = ['boss-03', 'boss-05', 'boss-06']
    mini_ops = ['boss-02']
    bad_ops = ['baddie-00', 'baddie-00', 'baddie-01', 'baddie-01', 'baddie-04']
    
    # These are fairly arbitrary and will probably need tweaking
    UPGRADE_MULT = 1.3
    BUDGET_MULT = .8
    BOSS_DIVIDE = 3
    CHOOSE_MINI = 30
    HEALTH_OVER = 5
        
    def __init__(self, gstate, level):
        self.gstate = gstate
        self.name = 'Mission of Doom: %s' % level
        self.talk = []
        self.description = ''
        self.difficulty = level * random.randint(1000, 1330)
        upgrade = self.difficulty / (level**1.15 / level) * self.UPGRADE_MULT
        self.bonus = {k: int(upgrade / self.bonuses[k]) for k in self.bonuses}
        self.mk_baddies()
    
    def mk_baddies(self):
        budget = int(self.difficulty * self.BUDGET_MULT)
        
        # Find out what boss to use first
        boss_budget = budget // self.BOSS_DIVIDE
        # Bosses are stored from cheapest to most expensive
        for boss in self.boss_ops[::-1]:
            boss = self.gstate.data.get_code(boss)
            if boss.value <= boss_budget:
                break
        
        # If the budget was large enough to "buy" one of the bosses, we will 
        # have it here. Otherwise, the last boss to be iterated (read: the 
        # cheapest) will be here.
        budget -= boss.value
        
        mini_classes = [self.gstate.data.get_code(b) for b in self.mini_ops]
        bad_classes = [self.gstate.data.get_code(b) for b in self.bad_ops]
        
        cheap = min(bad_classes, key=lambda b: b.value).value
        
        factor = Vector(self.gstate.area.size) / 10.
        locs = set()
        y = -4
        self.baddies = []
        
        while budget >= cheap:
            do_mini = not random.randint(0, self.CHOOSE_MINI)
            if do_mini:
                bad = random.choice(mini_classes)
            else:
                bad = random.choice(bad_classes)
            
            if bad.value > budget:
                # Woops, this one is too expensive!
                continue
            
            if not random.randint(0, 13):
                y -= random.randint(7, 15)
            elif random.randint(0, 3):
                y -= random.randint(1, 6)
            
            x = random.randint(2, 8)
            while (x, y) in locs:
                if random.randint(0, 3):
                    x = random.randint(2, 8)
                else:
                    x = random.randint(1, 9)
            loc = (x, y)
            
            budget -= bad.value
            locs.add(loc)
            self.baddies.append(bad(loc * factor, self.gstate))
        
        # The boss is started in the middle of the screen, hopefully about two 
        # screens behind the last baddie. The multiplier on y takes into 
        # account the fact that bosses travel faster than most baddies.
        boss_loc = Vector(5, y * 1.5 - 20)
        
        boss = boss(boss_loc * factor, self.gstate, speed=6)
        boss.health += budget * self.HEALTH_OVER
        self.baddies.append(boss)


class ArcadeLevelBank(object):
    lobj = ArcadeLevel
    
    def __init__(self, gstate):
        self.gstate = gstate
    
    def get_level(self, level):
        return ArcadeLevel(self.gstate, level)
    
    def exists(self, level):
        return True


class Level(object):
    def __init__(self, gstate, title, meta={}, bonus={}, bads=[]):
        self.gstate = gstate
        self.bonus = bonus
        self.title = title
        self.description = meta['desc']
        self.talk = meta['talk']
        self.name = meta['name']
        self.baddies = []
        factor = Vector(gstate.area.size) / 10.
        for bad in bads:
            loc = [int(f) for f in (factor * bad[1])]
            self.baddies.append(
              gstate.data.get_code(bad[0])(loc, gstate, *bad[2:]))


class LevelBank(object):
    lobj = Level
    def __init__(self, gstate):
        self.levels = {}
        self.sets = set()
        self.gstate = gstate
    
    def load_sets(self, dirs=[]):
        """
        Call this to load levels from each dir in @dirs.
        """
        ls = os.listdir
        join = os.path.join
        for d in dirs:
            contents = ls(d)
            for t in contents:
                first = join(d, t)
                if t.startswith('.') or os.path.isfile(first):
                    continue
                t_l = t.lower()
                for fl in ls(first):
                    full = join(first, fl)
                    if fl.startswith('.') or os.path.isdir(full):
                        continue
                    fl_n = fl.lower().rsplit('.', 1)[0]
                    ty = pyramid.guess_type(full)
                    if ty == pyramid.T_CODE and fl_n == '__init__':
                        self.load_it(d, t)
    
    def load_it(self, path, package):
        """
        Used internally when loading levels. You should probably use 
        load_sets().
        """
        sys.path = [path] + sys.path
        lvls = importlib.import_module(package).levels
        del sys.path[0]
        for L in lvls:
            # Everything is actually stored in lowercase
            rep = L.lower()
            self.sets.add(rep.split('.')[0])
            self.levels[rep] = lvls[L]
    
    def get_level(self, level):
        L = level.lower()
        return self.lobj(self.gstate, L, *self.levels[L])
    
    def exists(self, lvl):
        lvl = lvl.lower()
        if '.' in lvl:
            # a specific level
            return lvl in self.levels
        else:
            # a levelset
            print self.sets
            return lvl in self.sets


class ScrollingBG(pygame.Surface):
    def __init__(self, image, logo, speed=2):
        self.base = image
        self.rect = image.get_rect()
        pygame.Surface.__init__(self, self.rect.size)
        self.blit(self.base, (0, 0))
        self.blit
        self.height = self.rect.h
        self.pos = 0
        self.speed = speed
        self.logo = logo
        self.lrect = self.logo.get_rect(center = self.rect.center)
        self.showl = True
    
    def update(self):
        self.fill((0, 0, 0))
        self.pos += self.speed
        self.pos %= self.height
        self.blit(self.base, (0, self.pos))
        self.blit(self.base, (0, self.pos - self.height))
        if self.lrect.colliderect(self.rect) and self.showl:
            self.lrect.y += self.speed
            self.blit(self.logo, self.lrect)
