"""
This is the original Celestron codepack.
"""

import os
import random

import pygame
from pygame.constants import *
from pgpu.compatibility import range
import pgpu.math_utils as p_mu
from pgpu.math_utils import Vector
from pygu.common import center_blit


def get_objects(callwith):
    """
    Feel free to use this!
    """
    import importlib
    s = importlib.import_module(os.path.abspath(__file__).split(os.sep)[-2])
    
    return (s.__dict__[k] for k in dir(s) 
            if isinstance(getattr(s.__dict__[k], 'title', None), basestring))
get_objects.title = 'base.loader'


class Animation(pygame.sprite.Sprite):
    title = 'base.animation'
    
    def __init__(self, rect, life, gstate):
        pygame.sprite.Sprite.__init__(self, gstate.get_group('anims'))
        self.life = life
        self.gstate = gstate
        self.rect = rect
        self.sequence = self._sequence()
        self.ln = len(self.sequence)
        self.jolt = float(self.life) / len(self.sequence)
        self.go = 0
    
    def update(self, mult=1):
        self.life -= mult
        self.image = self.sequence[int(round(self.go * self.jolt)) % self.ln]
        self.go += mult
        if self.life <= 0:
            self.kill()
    
    def _sequence(self):
        """
        _sequence() is run during __init__() to get a list of images for the 
        animation.
        """
        assert False, 'You must define this!'


class StaticDeathAnim(Animation):
    title = 'static-death-anim'
    base = 'enemyexplo'
    
    def _sequence(self):
        return [self.gstate.data.get_image(self.base, 'explosions')]


class ResizingDeathAnim(StaticDeathAnim):
    title = 'resize-death-anim'
    
    def _sequence(self):
        pic = StaticDeathAnim._sequence(self)[0]
        pw, ph = pic.get_size()
        rw, rh = self.rect.w, self.rect.h
        sizer = min(float(pw) / rw, float(ph) / rh)
        atime = self.life // 2
        # hack that uses the numeric underlayment of booleans
        aplus = not atime * 2 == self.life
        mod = list(range(1, atime + 1 + aplus)) + list(range(atime, 0, -1))
        amod = 1. / max(mod)
        smod = amod / sizer
        seq = []
        for i in range(1, self.life + 1):
            vec = Vector((pw, ph)) * smod * mod[i - 1]
            tup = (max(int(vec.x), 2), max(int(vec.y), 2))
            img = pygame.transform.smoothscale(pic, tup)
            ref = pygame.surfarray.pixels_alpha(img) 
            ref *= amod * mod[i - 1]
            del ref
            targ = pygame.Surface(max((rw,), (rh,)) * 2, SRCALPHA)
            center_blit(targ, img)
            seq.append(targ)
        return seq


class Missile(pygame.sprite.Sprite):
    """
    A basic missile. Many missiles can be easily created by subclassing this 
    one, then editing the class data (img, damage, etc.).
    """
    
    title = 'base.missile'
    img = 'heroammo01'
    damage = 50
    invincible = False
    interval = 5
    explode = 'explo'
    sound = 'explostd'
    animlife = 7
    
    def __init__(self, loc, owner, bearing=0, speed=10):
        pygame.sprite.Sprite.__init__(self, owner.missiles)
        
        self.gstate = owner.gstate
        self.image = self.gstate.data.get_image(self.img, 'missiles')
        
        class MyAnim(ResizingDeathAnim):
            base = self.explode
        self.danim = MyAnim
        
        self.sound = self.gstate.data.get_sound(self.sound, 'sounds')
        if speed < 0:
            self.image = pygame.transform.flip(self.image, False, True)
        
        self.rect = self.image.get_rect().move(tuple(loc))
        self.rect.move_ip(-int(round(self.rect.width / 2.)), 0)
        self.area = owner.area
        self.rect = self.rect.clamp(self.area)
        self.bearing = bearing
        self.speed = speed
        self.dead = 0
        self.nohurt = {}
        self.mask = pygame.mask.from_surface(self.image)
    
    def update(self, mult=1):
        self.move(mult)
    
    def move(self, mult=1):
        #Print(self.bearing, self.speed)
        for t in self.nohurt:
            if not self.nohurt[t]:
                continue
            self.nohurt[t] += mult
            if self.nohurt[t] > 9:
                self.nohurt[t] = 0
        self.rect.move_ip(self.bearing * mult, -self.speed * mult)
        if not self.area.contains(self.rect):
            self.kill()
    
    def hit(self, t):
        if t in self.nohurt and self.nohurt[t]:
            return
        self.destruct()
        if not self.invincible:
            self.kill()
        else:
            self.nohurt[t] = 1
    
    def destruct(self):
        self.sound.play()
        self.danim(self.rect, self.animlife, self.gstate)


class HomingMissile(Missile):
    """
    Give this missile a target ship and it will home in on it!
    """
    
    title = 'base.homing-missile'
    life = 100  # set to -1 for infinite life
    turn = 5
    
    def __init__(self, loc, owner, target, speed=10):
        Missile.__init__(self, loc, owner, 0, speed=speed)
        self.target = pygame.sprite.GroupSingle(target)
        self.fspeed = speed
        self.first = True
    
    def update(self, mult=1):
        if self.life:
            self.life -= mult
        if not self.target or self.life <= 0:
            self.destruct()
            self.kill()
        loc = Vector(self.rect.center)
        tgt = Vector(self.target.sprite.rect.center)
        dif = loc - tgt
        try:
            bearing, self.speed = p_mu.legs(self.fspeed * mult, dif)
        except ZeroDivisionError:
            bearing = self.fspeed
            self.speed = 0
        if self.first:
            self.first = False
            self.bearing = -bearing
        else:
            bchange = min(max(-self.turn, self.bearing - bearing), self.turn)
            self.bearing -= bchange
        if dif.y < 0:
            self.bearing *= -1
        else:
            self.speed *= -1
        #Print(self.bearing, self.speed)
        self.move()


class BMissile00(Missile):
    title = 'bmiss-00'
    img = 'enemyammo00m'
    explode = 'enemyammoexplo00m'
    damage = 49
    interval = 6


class BMissile00H(HomingMissile):
    title = 'bmiss-00h'
    img = 'enemyammo00m'
    explode = 'enemyammoexplo00m'
    damage = 40
    interval = 5
    turn = 7


class BMissile01(Missile):
    title = 'bmiss-01'
    img = 'enemyammo01'
    explode = 'enemyammoexplo01'
    damage = 46
    interval = 4


class BMissile02(Missile):
    title = 'bmiss-02'
    img = 'enemyammo02m'
    explode = 'enemyammoexplo02m'
    damage = 59
    interval = 3


class BMissile03(Missile):
    title = 'bmiss-03'
    img = 'enemyammo03m'
    explode = 'enemyammoexplo03m'
    damage = 18
    interval = 1


class BMissile04(Missile):
    title = 'bmiss-04'
    img = 'enemyammo04m'
    explode = 'enemyammoexplo04m'
    damage = 41
    interval = 4


class BMissile06S(Missile):
    title = 'bmiss-06s'
    img = 'ammo06s'
    damage = 16
    interval = 3


class HMissile00(Missile):
    title = 'hmiss-00'
    img = 'heroammo00m2'
    explode = 'heroammoexplo00'
    damage = 18
    interval = 2


class HMissile01(Missile):
    title = 'hmiss-01'
    img = 'heroammo01m2'
    explode = 'heroammoexplo01'
    damage = 33
    interval = 4
    invincible = True


class HMissile02(Missile):
    title = 'hmiss-02'
    img = 'heroammo02'
    explode = 'heroammoexplo02'
    damage = 42
    interval = 5


class Weapon(object):
    """
    Do not base real weapons on Weapon() directly! Instead, use HWeapon() 
    or a similar Weapon() derivative.
    """
    
    title = 'base.weapon'
    
    def __init__(self, owner, missile, offset=None, double=False, **kw):
        self.owner = owner
        self.missile = missile
        self.offset = offset
        self.double = double
        self.bearing = kw.pop('bearing', 0)
        self.kw = kw
    
    def fire(self):
        if self.offset == None:
            self.missile(self.owner.rect.midtop, self.owner, self.bearing, 
                         **self.kw)
        else:
            ofr = self.offset
            ofl = (-self.offset[0], self.offset[1])
            if self.double:
                uno = dos = 1
            else:
                uno = random.randint(0, 1)
                dos = not uno
            if uno:
                self.missile(self.owner.rect.move(ofr).midtop, self.owner,
                        self.bearing, **self.kw)
            if dos:
                self.missile(self.owner.rect.move(ofl).midtop, self.owner, 
                        -self.bearing, **self.kw)


class HWeapon(Weapon):
    title = 'base.h-weapon'
    
    def __init__(self, owner, rounds=-1, missile=HMissile00, **kw):
        # if rounds == -1, then unlimited ammo is available
        self.rounds = rounds
        self.interval = kw.pop('interval', missile.interval)
        Weapon.__init__(self, owner, missile, **kw)
        self.counter = 0
        self.ready = True
    
    def fire(self, mult=1):
        if self.rounds == 0:
            return
        if self.ready:
            self.ready = False
            if self.rounds > 0:
                self.rounds -= 1
            #Print('%s round(s) left!' % self.rounds)
            self._fire()
        self.counter += mult
        if self.counter >= self.interval:
            self.ready = True
            self.counter -= self.interval
    
    def _fire(self):
        Weapon.fire(self)


class PWeapon(HWeapon):
    """
    A positionable, single barrel weapon.
    """
    
    title = 'base.p-weapon'
    
    def _fire(self):
        self.missile(self.owner.rect.move(self.offset).midtop, self.owner, 
                        self.bearing, **self.kw)


class AWeapon(PWeapon):
    """
    A positionable, single barreled weapon that will fire at the point given by 
    self._target().
    """
    
    title = 'base.a-weapon'
    
    def _target(self):
        """
        Should return target location. Has an interesting default.
        """
        return self.owner.area.midbottom
    
    def _fire(self):
        dkw = dict(self.kw)
        rsp = dkw.pop('speed')
        loc = Vector(self.owner.rect.move(self.offset).midtop)
        tgt = Vector(self._target())
        #Print(tgt)
        #Print(loc)
        try:
            dkw['bearing'], dkw['speed'] = p_mu.legs(rsp, loc - tgt)
        except ZeroDivisionError:
            dkw['bearing'] = rsp
            dkw['speed'] = 0
        if tgt.y <= loc.y:
            dkw['speed'] *= -1
        else:
            dkw['bearing'] *= -1
        #Print(dkw)
        self.missile(loc, self.owner, **dkw)


class HomingWeapon(HWeapon):
    """
    Fires homing missiles and homing missiles ONLY.
    """
    
    title = 'base.homing-weapon'
    
    def _target(self):
        """
        Must return a target ship.
        """
        assert False, 'You must implement this!'
    
    def _fire(self):
        self.missile(self.owner.rect.move(self.offset).midtop, self.owner, 
                    self._target(), **self.kw)


class HWeapon_A(HWeapon):
    title = 'hweap-a'
    
    def __init__(self, hero, rounds=-1, missile=HMissile00, offset=(13, 19), 
                 double=True, **kw):
        HWeapon.__init__(self, hero, rounds, missile, offset=offset, 
                         double=double, **kw)


class HWeapon_S(HWeapon):
    title = 'hweap-s'
    
    def __init__(self, hero, rounds=25, missile=HMissile01, offset=(20, 36), 
                 **kw):
        HWeapon.__init__(self, hero, rounds, missile, offset=offset, **kw)


class HWeapon_D(HWeapon):
    title = 'hweap-d'
    
    def __init__(self, hero, rounds=50, missile=HMissile02, offset=(28, 44), 
                 **kw):
        HWeapon.__init__(self, hero, rounds, missile, offset=offset, **kw)


class BWeapon00(HWeapon):
    title = 'bweap-00'
    
    def __init__(self, baddie, rounds=200, missile=BMissile00, offset=(20, 53), 
                 **kw):
        HWeapon.__init__(self, baddie, rounds, missile, offset=offset, **kw)


class BWeapon00H(HomingWeapon):
    title = 'bweap-00h'
    
    def __init__(self, baddie, rounds=200, missile=BMissile00H, 
                 offset=(20, 53), **kw):
        HWeapon.__init__(self, baddie, rounds, missile, offset = offset, **kw)
    
    def _target(self):
        """
        This homes in on a random hero!
        """
        return random.choice(self.owner.gstate.get_group('heroes').sprites())


class BWeapon01(HWeapon):
    title = 'bweap-01'
    
    def __init__(self, baddie, rounds=200, missile=BMissile01, offset=(20, 53), 
                 **kw):
        HWeapon.__init__(self, baddie, rounds, missile, offset=offset, **kw)


class BWeapon02(HWeapon):
    title = 'bweap-02'
    
    def __init__(self, baddie, rounds=200, missile=BMissile02, offset=None, 
                 **kw):
        HWeapon.__init__(self, baddie, rounds, missile, offset=offset, **kw)


class BWeapon03(AWeapon):
    title = 'bweap-03'
    
    def __init__(self, baddie, rounds=-1, missile=BMissile03, **kw):
                 AWeapon.__init__(self, baddie, rounds, missile, **kw)
    
    def _target(self):
        """
        This targets the center of a random hero!
        """
        return random.choice(self.owner.gstate.get_group('heroes').sprites()
                             ).rect.center
        

class BWeapon04(PWeapon):
    title = 'bweap-04'
    
    def __init__(self, baddie, rounds=200, missile=BMissile04, **kw):
        PWeapon.__init__(self, baddie, rounds, missile, **kw)


class BWeapon06S(PWeapon):
    title = 'bweap-06s'
    
    def __init__(self, baddie, rounds=-1, missile=BMissile06S, **kw):
        PWeapon.__init__(self, baddie, rounds, missile, **kw)


class SpawnShell(pygame.sprite.Sprite):
    title = 'spawnshell'
    
    def __init__(self, owner, loc, spawn, vector, power, **kw):
        pygame.sprite.Sprite.__init__(self, owner.spawn)
        
        owner.spawn.add(self)
        self.owner = owner
        self.loc = loc
        self.spawn = spawn
        self.vector = vector
        self.power = power
        self.kw = kw
        self.image = owner.gstate.data.get_image(spawn.image, 'ships')
        self.rect = self.image.get_rect()
        self.rect.center = loc
        self.area = self.owner.area
        self.health = spawn.health
        self.in_bounds = True
    
    def update(self, mult=1):
        self.move(mult)
    
    def move(self, mult=1):
        do_spawn = False
        pos = self.rect.move(self.vector * self.power)
        self.power //= 2
        
        if pos.left <= 0 or pos.right >= self.area.right or self.power < 1:
            do_spawn = True
        
        pos.left = max(0, pos.left)
        pos.right = min(self.area.right, pos.left)
        
        if do_spawn:
            #self.kw.setdefault('spawn_group', self.owner.spawn)
            self.spawn(pos.topleft, self.owner.gstate, **self.kw)
            self.kill()
        else:
            self.loc = pos
    
    def hit(self, m):
        # the spawn shell is made of an inpenetrable material.
        # Or something like that.
        return 0


class Spawner(object):
    """
    Spawns ships!
    """
    
    title = 'spawner'
    interval = 10
    
    def __init__(self, owner, spawn, vector, offset=Vector(0, 0), count=-1, 
                 power=8, **kw):
        self.owner = owner
        self.spawn = spawn
        self.vector = vector
        self.offset = offset
        self.count = count
        self.power = power
        self.kw = kw
        self.dospawn = random.randrange(0, self.interval)
    
    def fire(self, mult=1):
        if self.count == 0:
            return
        if not self.dospawn:
            if self.count > 0:
                self.count -= 1
            self._spawn()
        self.dospawn += 1
        self.dospawn %= self.interval
    
    def _spawn(self):
        loc = Vector(self.owner.rect.move(self.offset).midtop)
        SpawnShell(self.owner, loc, self.spawn, self.vector, self.power, 
                   **self.kw)


class Ship(pygame.sprite.Sprite):
    """
    Don't use this base class directly directly.
    """
    
    title = 'base.ship'
    s_group = None
    explode = 'enemyexplo'
    dead = 0
    sound = 'explostd'
    shield = 7          # damage done to other ships on collision
    animlife = 7
    
    def __init__(self, pos, gstate, speed):
        pygame.sprite.Sprite.__init__(self, gstate.get_group(self.group))
        
        self.image = gstate.data.get_image(self.image, 'ships')
        
        class MyAnim(ResizingDeathAnim):
            base = self.explode
        self.danim = MyAnim
        
        self.area = gstate.area
        self.rect = self.image.get_rect()
        try:
            self.rect.midtop = tuple(pos)
        except Exception as e:
            print(pos)
            raise e
        self.speed = speed
        self.missiles = gstate.get_group(self.m_group)
        if self.s_group != None:
            self.spawn = gstate.get_group(self.s_group)
        self.mask = pygame.mask.from_surface(self.image)
        self.sound = gstate.data.get_sound(self.sound, 'sounds')
        self.gstate = gstate
    
    def destruct(self):
        self.sound.play()
        self.danim(self.rect, self.animlife, self.gstate)
    
    def _hit(self):
        """
        Call this to check for deaths.
        """
        if self.health <= 0:
            self.destruct()
            self.kill()
            return self.value
        else:
            return 0
    
    def hit(self, m):
        if m.nohurt.get(self, 0):
            return 0
        self.health -= m.damage
        return self._hit()
        
    def collide(self, other):
        self.health -= other.shield
        return self._hit()


class Hero(Ship):
    title = 'hero-01'
    image ='hero'
    group = 'heroes'
    m_group = 'hero_missiles'
    
    health = 200
    shield = 41
    
    def __init__(self, pos, gstate, speed=5):
        Ship.__init__(self, pos, gstate, speed)
        
        self.weap_sp = HWeapon_A(self, speed=17, rounds=-1, interval=3)
        self.weap_a = HWeapon_A(self, rounds=100, speed=18, offset=None)
        self.weap_s = HWeapon_S(self, speed=15, rounds=60)
        self.weap_d = HWeapon_D(self, speed=16, rounds=30)
        
        # We keep the Hero in-bounds at all times
        self.in_bounds = True
    
    def move(self, xyfact=[0, 0], mult=1):
        mult = self.speed
        x, y = xyfact
        pos = self.rect.move(x * mult, y * mult)
        self.rect = pos.clamp(self.area)
    
    def fire(self, keystate={}, mult=1):
        if keystate[K_SPACE]:
            self.weap_sp.fire(mult)
        if keystate[K_a]:
            self.weap_a.fire(mult)
        if keystate[K_s]:
            self.weap_s.fire(mult)
        if keystate[K_d]:
            self.weap_d.fire(mult)


class HeroD02(Hero):
    title = 'hero-d02'
    image ='shipd02h'
    group = 'heroes'
    m_group = 'hero_missiles'
    
    health = 200
    shield = 41
    value = 1           # death hack!
    def __init__(self, pos, gstate, speed=5):
        Ship.__init__(self, pos, gstate, speed)
        
        self.weap_sp = HWeapon_A(self, rounds=100, speed=18, offset=(0, 3))
        self.weap_a = HWeapon_A(self, speed=17, rounds=-1, interval=3, 
                                offset=(13, 19))
        self.weap_s = HWeapon_S(self, speed=15, rounds=60, offset=(24, 40))
        self.weap_d = HWeapon_D(self, speed=16, rounds=30, offset=(29, 50))
        
        # We keep the Hero in-bounds at all times
        self.in_bounds = True


class Baddie(Ship):
    title = 'base.baddie'
    group = 'bads'
    s_group = 'bads'
    m_group = 'bad_missiles'
    animlife = 8
    LEFT = 0
    RIGHT = 1
    # Need this set by default; it could be True, but False is cheaper :P
    in_bounds = False
    
    def update(self, mult=1):
        self.move(mult)
        self.fire(mult)
    
    def move(self, mult=1):
        pos = self._move(mult)
        if pos.left <= 0:
            pos.left = 0
            self._hit_side(self.LEFT)
        elif pos.right >= self.area.right:
            pos.right = self.area.right
            self._hit_side(self.RIGHT)
        self.rect = pos
        # If we are visible at all, we can be shot at.
        self.in_bounds = self.rect.colliderect(self.area)
    
    def fire(self, mult=1):
        if self.rect.bottom < self.area.y:
            return
        for weap in self.weaps:
            if not random.randint(0, int(round(self.fire_break * mult))):
                weap.fire()


class MotionDown(object):
    """
    A plain mixin to give baddies downward motion.
    """
    
    title = 'bmix-mdown'
    
    def __init__(self, bosstarget=None):
        if bosstarget != None:
            bosstarget = self.area.h / 10. * bosstarget
        self.bosstarget = bosstarget
        self.locked = False
    
    def _move(self, mult=1):
        if not self.locked and self.bosstarget != None:
            if self.rect.centery > self.bosstarget:
                self.locked = True
        return self.rect.move(0, 0 if self.locked else self.speed * mult)
    
    def _hit_side(self, side):
        # This used to do the same thing as is done in Baddie().move(), except 
        # that this was being ignored. So let's not.
        pass


class MotionSideSweeper(MotionDown):
    """
    A mixin to give baddies side-to-side motion.
    """
    
    title = 'bmix-msidesweeper'
    
    def __init__(self, bosstarget=None):
        MotionDown.__init__(self, bosstarget)
        self.gright = random.randint(0, 1)
    
    def _move(self, mult=1):
        v = random.randint(0, 5)
        if v:
            fac = 1 if self.gright else -1
        else:
            fac = .5 if self.gright else -.5
        
        rect = MotionDown._move(self, mult)
        
        return rect.move(fac * self.speed * mult, 0)
    
    def _hit_side(self, side):
        self.gright = not side      # self.RIGHT is 1, self.LEFT is 0


class MotionSwoop(MotionSideSweeper):
    """
    A mixin to give baddies an up-and-down, side-to-side motion.
    """
    
    title = 'bmix-mswoop'
    
    def __init__(self, size=5, violence=1, bosstarget=None):
        MotionSideSweeper.__init__(self, bosstarget)
        self.size = size
        self.spot = 0
        self.motion = random.randint(0, 1)
        self.violence = violence
    
    def _move(self, mult=1):
        rect = MotionSideSweeper._move(self, mult)
        self.spot += 1
        self.spot %= self.size
        if not self.spot:
            self.motion = not self.motion
        factor = (1 if self.motion else -1) * mult
        return rect.move(0, int(round(factor * self.speed * self.violence)))


class Baddie00(Baddie, MotionDown):
    """
    A run-of-the-mill baddie that is more or less a turtle that fires homing 
    missiles slowly.
    """
    
    title = 'baddie-00'
    health = 300
    image = 'enemy00'
    fire_break = 20
    value = 20
    shield = 8
    
    def __init__(self, pos, gstate, speed=4):
        Baddie.__init__(self, pos, gstate, speed)
        MotionDown.__init__(self)
        
        self.weaps = [BWeapon00H(self, speed=-11, offset=(0, 63))]


class Baddie01(Baddie, MotionDown):
    """
    A weak baddie that fires swiftly to make up for its armour deficiencies.
    """
    
    title = 'baddie-01'
    health = 180
    image = 'enemy01m'
    fire_break = 10
    value = 23
    shield = 5
    
    def __init__(self, pos, gstate, speed=5):
        Baddie.__init__(self, pos, gstate, speed)
        MotionDown.__init__(self)
        
        self.weaps = [BWeapon01(self, speed=-11, double=True)]


class Baddie02(Baddie, MotionSwoop):
    """
    This baby-boss is all about shooting, and shooting FAST.
    """
    
    title = 'boss-02'
    health = 400
    image = 'enemy02'
    fire_break = 4
    value = 140
    shield = 11
    
    def __init__(self, pos, gstate, speed=5):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSwoop.__init__(self, 9, .2, 5.5)
        
        self.weaps = [BWeapon03(self, speed=-11, offset=(0, 64))]


class Baddie03(Baddie, MotionSideSweeper):
    """
    A baddie SO bad, it counts as a boss. Beware!
    """
    
    title = 'boss-03'
    health = 780
    image = 'enemy03m'
    fire_break = 4
    value = 200
    shield = 23
    animlife = 14
    
    def __init__(self, pos, gstate, speed=7):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSideSweeper.__init__(self, 4.98)
        
        self.weaps = [BWeapon00(self,speed=-10,double=True,offset=(54,120)),
                      BWeapon04(self, speed=-13, double=False, offset=(0, 84))]


class Baddie04(Baddie, MotionDown):
    """
    A very deadly baddie indeed!
    """
    
    title = 'baddie-04'
    health = 380
    image = 'enemy04'
    fire_break = 16
    value = 110
    shield = 10
    
    def __init__(self, pos, gstate, speed=4):
        Baddie.__init__(self, pos, gstate, speed)
        MotionDown.__init__(self)
        
        self.weaps = [BWeapon01(self, speed=-10, double=True, bearing=-1)]


class Baddie05(Baddie, MotionSideSweeper):
    """
    A deadly boss.
    """
    
    title = 'boss-05'
    health = 4500
    image = 'enemy05'
    fire_break = 3
    value = 900
    shield = 6
    animlife = 38
    def __init__(self, pos, gstate, speed=6):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSideSweeper.__init__(self, 3.9)
        
        self.weaps = [BWeapon03(self, speed=-13, offset=(0, 127)),
                BWeapon01(self, speed=-10, offset=(74, 120), double=True),
                BWeapon00(self, speed=-9, offset=(90, 120), double=True),
                BWeapon02(self, speed=-9, offset=(106, 120), double=True),
                BWeapon04(self, speed =-10, offset=(42, 78))]


class Baddie06(Baddie, MotionSwoop):
    """
    This deadly boss is packed with health and deadly weaponry. Even worse, it 
    spawns mini-baddies!
    """
    
    title = 'boss-06'
    health = 7000
    image = 'enemy06m'
    fire_break = 0
    value = 1500
    shield = 41
    animlife = 15
    
    def __init__(self, pos, gstate, speed=7):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSwoop.__init__(self, 10, .8, 3.85)
        
        self.weaps = [BWeapon04(self, speed=-11, offset=(30, 155)),
                      BWeapon04(self, speed=-11, offset=(-39, 138)),
                      Spawner(self, Baddie06S, Vector((4,-1)), 
                              Vector((98, 39)), 15, 2)]


class Baddie06S(Baddie, MotionSwoop):
    title = 'baddie-06s'
    health = 98
    image = 'enemya03m'
    fire_break = 9
    value = 7
    shield = 3
    
    def __init__(self, pos, gstate, speed=3):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSwoop.__init__(self, 7, 3, 6.5)
        
        self.weaps = [BWeapon06S(self, speed=-8, offset=(0, 24))]


class BaddieD01(Baddie, MotionSideSweeper):
    title = 'baddie-d01'
    health = 434
    image = 'shipd01'
    fire_break = 3
    value = 100
    shield = 9
    
    def __init__(self, pos, gstate, speed=5):
        Baddie.__init__(self, pos, gstate, speed)
        MotionSideSweeper.__init__(self)
        
        self.weaps = [BWeapon01(self, speed=-10, double=True, offset=(13, 62))]
