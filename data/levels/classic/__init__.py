levels = {
        ######################
        ##### Level 1 ########
        ######################
        
        'classic.01': (
          {'name': 'Level 1: All Is Quiet... Time for a Warm-up!', 
           'desc': ('Get some practice. You\'ve probably done this sort of ' 
                    'thing before, but it shouldn\'t be too hard either way.'), 
           'talk': [('Colonel Kardash: The Empire thinks we are subjugated. ' 
                     'Haha, just wait till they taste our new super-ship, '
                     'the Celestron!'),
                    'You: Aren\'t evil empires always mistaken?',
                    ('Colonel Kardash: No, unfortunately not. But most of the ' 
                     'time. And this will be one of those times.'),
                    'You: *snicker*',
                    ('Colonel Kardash: The Western Front should be a good ' 
                     'target. But hurry, we need the supplies that you will ' 
                     'capture!'),
                    'You: It shall be done. Prulum shall prevail!']},
          
          {'health': 1000, 'w0': 180, 'w1': 110, 'w2': 90},
          
          (('baddie-00', (4.85, -4.2)), ('baddie-00', (5, -5)), 
           ('baddie-00', (5.15, -5.8)), ('baddie-04', (5, -10)), 
           ('boss-02', (5, -18)), ('baddie-04', (7, -22)), 
           ('baddie-01', (4.5, -34)), ('baddie-01', (5.5, -34)), 
           ('baddie-01', (4, -35.5)), ('baddie-01', (6, -35.5)), 
           ('baddie-01', (3.5, -37)), ('baddie-01', (6.5, -37)), 
           ('baddie-04', (3, -38)), ('boss-03', (5, -85)))),
        
        ######################
        ##### Level 2 ########
        ######################
        
        'classic.02': (
          {'name': 'Level 2: No More Mr. Nice Guy!', 
           'desc': ('You should have seen this coming. Hopefully you aren\'t ' 
                    'totally depleted...'), 
           'talk': [('Colonel Kardash: The Empire seems to be investigating ' 
                     'the recent loss of radio transmissions from our sector ' 
                     ' --'),
                    'You: I wonder why. *laughs*',
                    ('Colonel Kardash: Did I mention that this recon party ' 
                     'is, ahem, no small probe?'),
                    'You: No? But what does that have to do with me?',
                    'Colonel Kardash: You must stop them!',
                    ('You: *gulp* How? My ship is still weak from that supply ' 
                     'raid.'),
                    ('Colonel Kardash: When you reach your base, all the ' 
                     'supplies that we could spare from HQ\'s defenses will ' 
                     'be awaiting you. They will have to be enough.'),
                    'You: HQ\'s defenses?',
                    ('Colonel Kardash: We will have to deal with any enemy ' 
                     'ships that get past, at great cost. Besides this, if ' 
                     'headquarters is destroyed, an auto-destruct mechanism ' 
                     'in your ship will be triggered, to prevent the ship ' 
                     'from falling into the wrong hands. Unfortunately, it ' 
                     'will also keep you from falling into the wrong hands. ' 
                     'So, be careful, but don\'t allow many enemies past!'),
                    'You: ...',
                    'Colonel Kardash: Hendric?',
                    'You: I-I will try!',
                    ('Colonel Kardash: That\'s the spirit! Prulum shall '
                     'prevail!')]},
          
          {'health': 3000, 'w0': 300, 'w1': 200, 'w2': 200}, 
        
          (('baddie-00', (4.1, -4)), ('baddie-00', (5, -5)), 
           ('baddie-00', (5.9, -6)), 
           
           ('baddie-04', (5, -11)), 
           ('boss-02', (1, -18)), 
           ('baddie-04', (7, -22)), 
           
           ('baddie-01', (4, -34)), ('baddie-01', (5.5, -30)), 
           ('baddie-01', (4, -36)), ('baddie-01', (6, -32)), 
           ('baddie-01', (3.5, -37)), ('baddie-01', (6.5, -37)), 
        
           ('baddie-04', (3, -39)), 
           ('baddie-00', (9, -45)), 
           ('boss-02', (4, -60)), 
           ('baddie-01', (5, -90)),
        
           ('baddie-01', (5, -110)), ('baddie-00', (4, -115)),
           ('baddie-00', (6, -115)), ('baddie-01', (3, -120)),
           ('baddie-01', (7, -120)), ('baddie-04', (2, -125)),
           ('baddie-04', (8, -125)), 
        
           ('baddie-01', (2, -135)), ('baddie-01', (3, -155)), 
           ('baddie-01', (4, -170)), ('baddie-01', (5, -185)), 
           ('baddie-01', (6, -200)), ('baddie-01', (7, -215)),
           ('baddie-01', (8, -230)),
        
           ('baddie-01', (4, -255)), ('baddie-01', (5, -255)),
           ('baddie-01', (6, -255)), ('baddie-01', (4, -260)), 
           ('baddie-01', (5, -260)), ('baddie-01', (6, -260)), 
           ('baddie-01', (4, -265)), ('baddie-01', (5, -265)), 
           ('baddie-01', (6, -265)), ('baddie-01', (4, -270)), 
           ('baddie-01', (5, -270)), ('baddie-01', (6, -270)), 
           ('baddie-04', (5, -230)), 
           ('boss-05', (5, -400)))),
        
        ######################
        ##### Level 3 ########
        ######################
        
        'classic.03': (
          {'name': 'Level 3: To Be Continued...', 
           'desc': 'That\'s right. This level isn\'t finished yet.',
           'talk': [('Colonel Kardash: I have some good news, and some bad ' 
                     'news. Which do you want to hear first?'),
                    'You: ...',
                    ('Colonel Kardash: It\'s OK, Hendric. You only have to buy' 
                     'so much time. At any rate, the good news is that we have' 
                     'planted a spy within the Empire!'),
                    'You: How in the galaxy? That\'s... almost unbelievable.',
                    ('Colonel Kardash: That doesn\'t matter. But on to the bad' 
                     'bad news. The spy reports that the Empire is sending a ' 
                     'carrier this way!'),
                    'You: Is that worse than the frigate I fought last time?', 
                    ('Colonel Kardash: Unfortunately, yes. Besides being ' 
                     'stronger, a carrier, as the name suggests, carries ' 
                     'other ships, which it can release in combat.'),
                    ('You: Great. I suppose that I\'ll have to keep this mob ' 
                     'of ships away from headquarters...'),
                    ('Colonel Kardash: Fortunately, the answer to that ' 
                     'question is "no". The spy reports that the ships ' 
                     'launched are weak things, unable to trek through the ' 
                     'galaxy alone. Hence the need for a carrier. Or perhaps ' 
                     'the other way around. Though our spy is not certain, he ' 
                     'believes that the ships are intended to protect the ' 
                     'carrier, and to provide a sort of diversionary ' 
                     'armament. So, focus on the carrier, not the ships it ' 
                     'launches. TODO point: At the moment, it shouldn\'t be ' 
                     'hard to take it out.')]},
          
          {'health': 5000, 'w0': 350, 'w1': 240, 'w2': 230}, 
        
          (('baddie-04', (5, -5)),
           ('boss-06', (6, -50)))),
        
        ######################
        ##### TODO: Level 4 ##
        ######################
        }
