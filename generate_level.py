#! /usr/bin/env python

"""
generate_level.py
=================

Generate Celestron levels from images
-------------------------------------

*** Usage ***

$ generate_level.py -h
$ generate_level.py --help
Display help and quit.

$ generate_level.py image color baddie [color baddie...]
Generate a level from the given image and color-baddie conversion pairs. The 
level should be ten pixels wide.

$ generate_level.py leveltest.png red baddie-00 blue baddie-01 yellow baddie-04 
  green boss-05
Test this script on the included leveltest.png image.
"""

import sys
import json

import pygame
from pgpu.compatibility import range
from pgpu.iter_utils import section


def analyzer(surf, conversions):
    bads = []
    # We want to orient the image in the same way as the level, but the image 
    # location system (unfortunately) is upside-down. So it shall be flipped.
    for y_loc, img_y in enumerate(range(surf.get_height() - 1, -1, -1)):
        # Each pixel is a tenth of a screen wide and tall, this is to say, one 
        # Level Positioning Unit. This image system does not do fractions; if 
        # such fine-tuning is desired, it can be done with the blank-filling 
        # after the baddie list is put into a file. We will ignore any baddies 
        # which the image creator was foolish enough to put outside this width.
        for x in range(10):
            # Get the baddie that this pixel's color corresponds to, or None. 
            # Imagine this in the same way as you would re.match(): if we have 
            # a match, return it; otherwise, return None.
            bad_type = conversions.get(tuple(surf.get_at((x, img_y))), None)
            # If the pixel color did match, add the selected baddie at the 
            # current position.
            if bad_type:
                bads.append([bad_type, (x, -y_loc)])
    
    return bads


def get_conversions(args):
    conversions = {}
    for color, baddie in section(args, 2):
        try:
            # 'red', '#00ff00', '0xffff00ff' --> Color()
            color = pygame.color.Color(color)
        except ValueError:
            # '0,0,255' --> '[0,0,255]' --(json)-->  Color()
            color = pygame.color.Color(*json.loads('[' + color + ']'))
        
        # All of our colors must be changed to tuples to allow use as dict keys
        conversions[tuple(color)] = baddie
    
    return conversions


def main(args):
    try:
        arg0 = args.pop(0)
    except IndexError:
        arg0 = '-h'
    
    if arg0 in ['-h', '--help']:
        return __doc__
    
    surf = pygame.image.load(arg0)
    conversions = get_conversions(args)
    baddies = analyzer(surf, conversions)
    
    return json.dumps(baddies)


if __name__ == '__main__':
    pygame.init()
    print(main(sys.argv[1:]))
